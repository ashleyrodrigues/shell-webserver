#!/bin/sh


# Http request handler Ashley Rodrigues UNSW

read http_request || exit 0

targetLoc="$(echo $http_request | cut -d " " -f2)"
targetFile="$(echo $targetLoc | egrep -o "[^/]*$" | grep "[^ ]*")"
srvrRoot="$HOME/public_html"
targetFileExt=$(echo $targetFile | egrep -o "[^.]*$" | egrep -o "^[a-z]*")
srvrFileLoc="$(echo "${srvrRoot}${targetLoc}" | tr -d '\n')"

#check if the location is a valid file location on the system
if [ -f "$srvrFileLoc" ]; then
    isImage=$(egrep image /etc/mime.types | egrep -w "$targetFileExt")
    # Applies the appropriate action on the file depending upon its extension
    if [ "$targetFileExt" = "html" ]; then
        
        status_line="HTTP/1.0 200 OK"
        content_type="text/html"
        content="contents:$srvrFileLoc $(cat $srvrFileLoc)"
        content_length=`echo "$content"|wc -c`


        echo "HTTP/1.0 200 OK"
        echo "Content-type: $content_type"
        echo "Content-length: $content_length"
        echo
        echo "$content"
        exit 0
    # If an image
    elif [ "$isImage" ]; then
        partLocFile="$(echo "$srvrFileLoc" | sed 's/\/public_html\///')"          
        locFile="http://localhost:2041/$partLocFile"

        status_line="HTTP/1.0 200 OK"
        content_type="image/jpeg"
        content_length=`cat "$srvrFileLoc"|wc -c`

        # Echo uses a new line whereas cat preserves previous
        # data formatting

        echo "HTTP/1.0 200 OK"
        echo "Content-type: $content_type"
        echo "Content-length: $content_length"
        echo
        cat "$srvrFileLoc"
        exit 0

    else    
        var=`echo "$targetFileExt" | wc -c`
        status_line="HTTP/1.0 200 OK"
        content_type="text/plain    "
        content="$(cat $srvrFileLoc)"
        content_length=`echo "$content"|wc -c`


        echo "HTTP/1.0 200 OK"
        echo "Content-type: $content_type"
        echo "Content-length: $content_length"
        echo
        echo "$content"
        exit 0
    fi
# Handles directories
elif [ -d $srvrFileLoc ]; then
    # If directory contains an index.html, open the file
    if [ -f $srvrFileLoc/index.html ]; then
        status_line="HTTP/1.0 200 OK"
        content_type="text/html"
        content="contents: $srvrFileLoc $(cat $srvrFileLoc/index.html)"
        content_length=`echo "$content"|wc -c`

        echo "HTTP/1.0 200 OK"
        echo "Content-type: $content_type"
        echo "Content-length: $content_length"
        echo
        echo "$content"
        exit 0

    else
        # If index.html is not found list all the files/ directories in the directory
        status_line="HTTP/1.0 200 OK"
        content_type="text/html"
        content="hello"
        content_length=`echo "$content"|wc -c`

        webPage='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
        "http://www.w3.org/TR/REC-html40/loose.dtd">
        <HTML>
         <HEAD>
          <TITLE>Index of /~cs2041cgi/15s2/lab/sh/webserver/example_directory</TITLE>
         <META NAME="generator", CONTENT="mod_autoindex"> </HEAD>
         <BODY bgcolor="#ffffff" text="#000000">

        <TABLE><TR><TD bgcolor="#ffffff" class="title">
        <FONT size="+3" face="Helvetica,Arial,sans-serif">
        <B>Index of /~cs2041cgi/15s2/lab/sh/webserver/example_directory</B></FONT>

        </TD></TR></TABLE><PRE><IMG border="0" src="/icons/blank.gif" ALT="     " HEIGHT="22" WIDTH="20"> <A HREF="?N=D">Name</A>                    <A HREF="?M=A">Last modified</A>       <A HREF="?S=A">Size</A>  <A HREF="?D=A">Description</A>
        <HR noshade align="left" width="80%">
        '
        if [ $srvrFileLoc = "/" ]; then
            webPage="$webPage<IMG border="0" src="/icons/back.gif" ALT="[DIR]" HEIGHT="22" WIDTH="20"> <A HREF="http://localhost:2041/">Parent Directory</A>"
        else
             srvrFileParent="$(echo $targetLoc | sed 's/.*/$//')"
             webPage="$webPage<IMG border="0" src="/icons/back.gif" ALT="[DIR]" HEIGHT="22" WIDTH="20"> <A HREF="http://localhost:2041$srvrFileParent">Parent Directory</A>"
        fi
        for file in $srvrFileLoc/*
        do
            nFile="$(echo "$file" | egrep -o "[a-zA-Z0-9.]*$")"
            escSrvrRoot=$(echo "$srvrRoot" | sed 's/\//\\\//g')
            locFile="$(echo "$file" | sed "s/$escSrvrRoot\///")"
            if [ $nFile ]; then
                webPage="$webPage\n<IMG border="0" src="/icons/text.gif" ALT="[TXT]" HEIGHT="22" WIDTH="20"> <A HREF="http://localhost:2041/$locFile">$nFile</A>"
            
            fi
        done

        webPage="$webPage\n</PRE><HR noshade align="left" width="80%">
        <ADDRESS>Apache/1.3.34 Server at cgi.cse.unsw.edu.au Port 80</ADDRESS>
        </BODY></HTML>$loc"

        echo "HTTP/1.0 200 OK"
        echo "Content-type: $content_type"
        echo "Content-length: $content_length"
        echo
        echo "$webPage"
        exit 0
    fi
else
    # If fail is not found
    status_line="HTTP/1.0 404 Not Found"
    content_type="text/plain"
    content="Not Found"
    content_length=`echo "$content"|wc -c`


    echo "HTTP/1.0 200 OK"
    echo "Content-type: $content_type"
    echo "Content-length: $content_length"
    echo
    echo "$content"
    exit 0
fi
  


